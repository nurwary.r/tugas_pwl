var mongoose = require('mongoose');
var Perlombaan = require('../models/Perlombaan');

var perController = {};

perController.find = function(err, res){
    var perlombaan = new Perlombaan(req.body);
    
    perlombaan.find({}, function(err, perl){
        console.log(perl);
        res.render('peserta', {perlombaan: perl, title: 'crud perlombaan'});
    }).select('_id');
};

perController.save = function(req, res){
    console.log(req.body.nama);
    Perlombaan.find({nama : req.body.nama}, function (err, docs) {
        if (docs.length){
            console.log('data sudah ada');
            res.redirect('../perlombaan');
        }else{
            var perlombaan = new Perlombaan(req.body);
    
            perlombaan.save(function(err){
                if(err){
                    console.log(err);
                    res.redirect('../perlombaan');
                }else{
                    console.log('save sukses');
                    res.redirect('../perlombaan');
                }
            });
        }
    });
};

module.exports = perController;