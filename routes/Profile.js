var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var profile = require('../models/Profile');
var Auth_mdw = require('../middlewares/auth');
var session_store;
// var multer = require('multer');
var path = require('path');
var app = express();


// router.get('/detail', Auth_mdw.check_login, function(req, res, next){
//     session_store = req.session;
//     res.send('respond with a resource');
// });

router.get('/detail', function(req, res, next){
    session_store = req.session;
    foto_profile = app.use('/public/images', express.static(__dirname + '/public/images'))
    profile.findOne({}, function(err, perl){
        console.log(perl);
        res.render('profile', { profile: perl, title: 'Profiles', sessions: session_store, foto:foto_profile});
    });
});


module.exports = router;
