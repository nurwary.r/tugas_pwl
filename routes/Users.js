var express = require('express');
var router = express.Router();
var profile = require('../models/Profile');
var Auth_mdw = require('../middlewares/auth');


/* GET users listing. */
router.get('/detail', function(req, res, next) {
  session_store = req.session;
    profile.find({}, function(err, perl){
        // console.log(perl);
        res.render('profile', { profile: perl, title: 'Profile', sessions: session_store});
    }).select('_id nama');
});


module.exports = router;
